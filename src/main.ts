import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index' // vuex
import './registerServiceWorker' // pwa
import './func/index' // 常用函数

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
