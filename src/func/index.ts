import fetch from './fetch' // 网络请求
import time from './time'
import hardware from './hardware'
import notify from './notify'
// import {mouseDrag} from './mouse'
// array是特殊方法, 直接修改了array原型链
// import {arr} from './array'
// arr.add('max', 'min', 'remove')
const func = {
	fetch,
	time,
	hardware,
	notify,
	// mouseDrag,
}
export default func
