class arr {
	constructor(needs) {
		needs.forEach(v => {
			this[v]()
		})
	}
	static add(...needs) {
		return new arr(needs)
	}
	max() {
		// arr.max 直接得到最大值
		if (!Array.prototype.max) {
			Object.defineProperty(Array.prototype, 'max', {
				get() {
					return Math.max.apply(null, this)
				},
				set(v) {
					if (v > this.max) {
						this.push(v)
					}
				},
			})
		}
	}
	min() {
		// arr.min 直接得到最小值
		if (!Array.prototype.min) {
			Object.defineProperty(Array.prototype, 'min', {
				get() {
					return Math.min.apply(null, this)
				},
				set(v) {
					if (v < this.min) {
						this.push(v)
					}
				},
			})
		}
	}
	remove() {
		// arr.remove(v, false) 得到清除v后的新数组(不改变原数组)
		// arr.remove(v, true) 得到清除v后的数组(改变原数组)
		if (!Array.prototype.remove) {
			Object.setPrototypeOf(Array.prototype, {
				remove(v, local) {
					if (local) {
						let j = 0
						for (let [i, len] = [0, this.length]; i < len; i++) {
							if (v !== this[i]) {
								this[j] = this[i]
								j++
							}
						}
						this.length = j
						return this
					} else {
						let re = []
						for (let [i, len] = [0, this.length]; i < len; i++) {
							const a = this[i]
							if (v !== a) {
								re.push(a)
							}
						}
						return re
					}
				},
			})
		}
	}
}
export {arr}
