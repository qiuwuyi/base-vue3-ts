interface State {
	some: number
}
const test = {
	state: {
		some: 2333,
	},
	mutations: {
		test_some(state: State, n: number) {
			state.some = n
		},
	},
}
export {test}
